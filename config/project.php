<?php

return [
    'vehicle_types' => [
        0 => "Select vehicle type",
        'Bike' => "Bike",
        'Motorbike' => "Motorbike",
        'Car' => "Car"
    ]
];