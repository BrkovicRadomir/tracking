<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth routes
Route::get('/', ['as' => "auth.login", "uses" => 'Auth\LoginController@login']);
Route::post('/', ['as' => "auth.authorization", "uses" => 'Auth\LoginController@authorization']);
Route::get('/logout', ['as' => "auth.logout", "uses" => 'Auth\LoginController@logout'])->middleware('auth');

// Registration
Route::get('/registration', ['as' => "registration.form", 'uses' => 'Auth\RegisterController@form']);
Route::post('/registration', ['as' => "registration.store", 'uses' => 'Auth\RegisterController@registration']);


// Trips

Route::resource('trips', "TripController")->middleware('auth');

