<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ isset($title) ? $title : "Tracking application" }}</title>
    <link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">

    @yield("head")
</head>
<body>
@if(Auth::user())
    <header>
        <div class="container">
            <div class="row">
                <div class="col-10">
                    Welcome {{ Auth::user()->first_name }}
                </div>
                <div class="col-2">
                    <a href="{{ route('auth.logout') }}" class="btn btn-info">Logout</a>
                </div>
            </div>
        </div>
    </header>

@endif

<section>
    <div class="container">
        @yield('content')
    </div>
</section>

@yield('js-scripts')
</body>
</html>