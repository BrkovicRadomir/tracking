@extends('layout.main', ['title' => 'Login - Tracking app'])

@section('content')
    <div class="row">
        <div class="col-8 offset-2">

            @if ($message = Session::get('warning'))
                <div class="alert alert-warning alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif


            <h1 class="text-center">Login on tracking app</h1>
            {!! Form::open(['route' => "auth.authorization"]) !!}

                <div class="form-group  {{ $errors->first('email') ? 'has-error' : null }}">
                    <label for="email">Enter your email</label>
                    {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email']) !!}
                    <span class="form-control-feedback">{{ $errors->first('email') }}</span>
                </div>


            <div class="form-group  {{ $errors->first('password') ? 'has-error' : null }}">
                <label for="password">Enter your password</label>
                {!! Form::password('password', ['class' => 'form-control' , 'id' => 'password']) !!}
                <span class="form-control-feedback">{{ $errors->first('password') }}</span>
            </div>

             <div class="col-12">
                 <div class="row">
                     <div class="col-6">
                         <div class="form-group">

                             <input type="checkbox" name="remember_me" id="remember-me" value="1">
                             <label for="remember-me">Remember me?</label>
                         </div>
                     </div>

                     <div class="col-6">
                         <a href="{{ route('registration.form') }}">Create account</a>
                     </div>
                 </div>

             </div>


            <button type="submit" class="btn btn-primary">Login</button>
            {!! Form::close() !!}
        </div>
    </div>

@endsection


