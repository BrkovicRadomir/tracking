@extends('layout.main', ['title' => 'Create account - Tracking app'])

@section('content')
    <div class="row">
        <div class="col-8 offset-2">



            <h1 class="text-center">Create account on tracking app</h1>
            {!! Form::open(['route' => "registration.store"]) !!}

            <div class="form-group  {{ $errors->first('first_name') ? 'has-error' : null }}">
                <label for="first_name">Enter your first name</label>
                {!! Form::text('first_name', null, ['class' => 'form-control', 'id' => 'first_name']) !!}
                <span class="form-control-feedback">{{ $errors->first('first_name') }}</span>
            </div>

            <div class="form-group  {{ $errors->first('last_name') ? 'has-error' : null }}">
                <label for="last_name">Enter your last name</label>
                {!! Form::text('last_name', null, ['class' => 'form-control', 'id' => 'last_name']) !!}
                <span class="form-control-feedback">{{ $errors->first('last_name') }}</span>
            </div>

            <div class="form-group  {{ $errors->first('email') ? 'has-error' : null }}">
                <label for="email">Enter your email</label>
                {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email']) !!}
                <span class="form-control-feedback">{{ $errors->first('email') }}</span>
            </div>


            <div class="form-group  {{ $errors->first('password') ? 'has-error' : null }}">
                <label for="password">Enter your password</label>
                {!! Form::password('password', ['class' => 'form-control' , 'id' => 'password']) !!}
                <span class="form-control-feedback">{{ $errors->first('password') }}</span>
            </div>

            <div class="form-group  {{ $errors->first('password_confirmation') ? 'has-error' : null }}">
                <label for="password_confirmation">Enter your password</label>
                {!! Form::password('password_confirmation', ['class' => 'form-control' , 'id' => 'password_confirmation']) !!}
                <span class="form-control-feedback">{{ $errors->first('password_confirmation') }}</span>
            </div>



            <button type="submit" class="btn btn-primary">Submit</button>
            {!! Form::close() !!}
        </div>
    </div>

@endsection


