@extends('layout.main', ['title' => $trip->name.' - Edit'])

@section('content')
    <div class="row">
        <div class="col-8 offset-2">



            <h1 class="text-center">{{ $trip->name }} - Edit</h1>
            {!! Form::model($trip, ['method' => 'PATCH', 'route' => ['trips.update', $trip->id], 'files' => true]) !!}

            <div class="form-group  {{ $errors->first('name') ? 'has-error' : null }}">
                <label for="name">Trip name</label>
                {!! Form::text('name', $trip->name, ['class' => 'form-control', 'id' => 'name']) !!}
                <span class="form-control-feedback">{{ $errors->first('name') }}</span>
            </div>

            <div class="form-group  {{ $errors->first('vehicle') ? 'has-error' : null }}">
                <label for="vehicle">Vehicle type</label>
                {!! Form::select('vehicle', config('project.vehicle_types'), $trip->vehicle, ['class' => 'form-control', 'id' => 'vehicle']) !!}
                <span class="form-control-feedback">{{ $errors->first('vehicle') }}</span>
            </div>


            <div class="form-group  {{ $errors->first('points') ? 'has-error' : null }}">
                <label for="vehicle">Points</label>
                {!! Form::file('points', null) !!}
                <span class="form-control-feedback">{{ $errors->first('points') }}</span>
            </div>



            <button type="submit" class="btn btn-primary">Submit</button>
            {!! Form::close() !!}
        </div>
    </div>

@endsection


