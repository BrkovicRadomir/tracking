@extends('layout.main', ['title' => $trip->name])

@section('content')

    <div class="row">

        <div class="col-12">
            <h1>{{ $trip->name }} ({{ $trip->vehicle }}) </h1>
        </div>

        <div class="col-12" id="map"></div>
    </div>

@endsection

@section('js-scripts')
    <script>
        var  tripPlanCoordinates = [];

        @foreach($trip->points as $point)
            tripPlanCoordinates.push({
            lat: {{ $point->latitude }},
            lng: {{ $point->longitude }}
                });
        @endforeach

        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 10,
                center: {lat: tripPlanCoordinates[0]? tripPlanCoordinates[0].lat : 0,
                         lng: tripPlanCoordinates[0] ? tripPlanCoordinates[0].lng : 0},
                mapTypeId: 'terrain'
            });


            var tripPath = new google.maps.Polyline({
                path: tripPlanCoordinates,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });

            tripPath.setMap(map);

        }


    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API_KEY') }}&callback=initMap">
    </script>
@endsection