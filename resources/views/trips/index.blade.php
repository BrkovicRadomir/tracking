@extends('layout.main', ['title' => 'Trips'])

@section('content')

    <div class="row">

        <div class="col-10">
            <h1>Trips list</h1>
        </div>

        <div class="col-2">
            <a href="{{ route('trips.create') }}" class="btn btn-success">Add new trip</a>
        </div>

        @if($trips->count())
            <div class="col-12">
                {!! Form::open(['route' => "trips.index", 'method' => "get"]) !!}
                    <div class="row">
                        <div class="col-12">
                            <label for="query">Search</label>
                        </div>
                        <div class="col-10">
                            <div class="form-group">

                                {!! Form::text('query', $query, ['id' => 'query', 'class' => "form-control" ]) !!}
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <button class="btn btn-primary">Search</button>
                            </div>
                        </div>
                    </div>


                {!! Form::close() !!}
            </div>

            <div class="col-12">
                {!! $trips->links() !!}
            </div>


            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Vehicle</th>
                        <th>Created at</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                @foreach($trips as $trip)

                    <tr>
                        <td>{{ $trip->name }}</td>
                        <td>{{ $trip->vehicle }}</td>
                        <td>{{ date('M d, Y', strtotime($trip->created_at)) }}</td>
                        <td>
                            <a href="{{ route('trips.show', $trip->id) }}" class="btn-primary btn left m-1"><i class="fa fa-eye"></i></a>
                            <a href="{{ route('trips.edit', $trip->id) }}" class="btn-info btn left m-1"><i class="fa fa-pencil"></i></a>

                            {!! Form::model($trip, ['method' => 'DELETE', 'route' => ['trips.destroy', $trip->id], 'onsubmit' => "return confirm('Are you sure you want to DELETE this trip?')", 'class' => "left m-1"]) !!}
                                <button type="submit" class="btn-danger btn"><i class="fa fa-trash"></i></button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-12">
                {!! $trips->links() !!}
            </div>

        @else
            <div class="col-12">
                <p>No one of trips doesn't found in database</p>
            </div>
        @endif

    </div>


@endsection