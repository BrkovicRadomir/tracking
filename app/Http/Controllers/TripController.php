<?php

namespace App\Http\Controllers;

use App\Exceptions\GPXIncorrectFormatException;
use App\Http\Requests\TripRequest;
use App\Services\TripPointService;
use App\Trip;
use Illuminate\Http\Request;

class TripController extends Controller
{
    protected $trip = null;


    public function __construct(Trip $trip)
    {
        $this->trip = $trip;
    }


    /**
     * Display a listing of the resource.
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $trips = $this->trip->where('user_id', \Auth::id());


        if($request->get('query')){
            $trips = $trips->where('name', "LIKE", "%".$request->get('query')."%");
        }

        $trips = $trips->paginate(20);

        $query = $request->get('query');

        return view('trips.index', compact('trips', 'query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('trips.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $request
     * @throws GPXIncorrectFormatException
     * @return \Illuminate\Http\Response
     */
    public function store(TripRequest $request)
    {
        $inputs = $request->except(['points']);
        $inputs['user_id'] = \Auth::id();
        $trip = $this->trip->create($inputs);

        TripPointService::create($trip->id, $request->file('points'));

        return redirect()->route('trips.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trip = $this->trip->with('points')->findOrFail($id);

        return view('trips.show', compact('trip'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trip = $this->trip->findOrFail($id);
        return view('trips.edit', compact('trip'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @throws GPXIncorrectFormatException
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $trip = $this->trip->with('points')->findOrFail($id);

        $trip->update($request->except(['points']));

        if($request->file('points')){
            $trip->points()->delete();
            TripPointService::create($trip->id, $request->file('points'));
        }

        return redirect()->route('trips.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trip = $this->trip->with('points')->findOrFail($id);
        $trip->points()->delete();
        $trip->delete();
        return redirect()->route('trips.index');
    }
}
