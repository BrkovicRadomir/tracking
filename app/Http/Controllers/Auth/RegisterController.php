<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\RegisterRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    /**
     * Display registration form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form(){
        return view('auth.register');
    }


    /**
     * Create new user and redirect them on his trips list
     * @param RegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function registration(RegisterRequest $request){
        $inputs = $request->except('password_confirmation');

        $user=  User::create($inputs);

        \Auth::loginUsingId($user->id);

        return redirect()->route('trips.index');
    }

}



