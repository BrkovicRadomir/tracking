<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\LoginAuthorizationRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Display login form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login(){
        if(!Auth::user()){
            return view('auth.login');
        } else {
            return redirect()->route('trips.index');
        }

    }


    /**
     * Handling authorization request from login form
     * @param LoginAuthorizationRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function authorization(LoginAuthorizationRequest $request){
        if(Auth::attempt($request->only('email', 'password'), $request->get('remember_me'))){
            return redirect()->route('trips.index');
        } else {
            return redirect()->route('auth.login')->with('warning', \Lang::get('auth.failed'));
        }
    }


    /**
     * Logout user and redirect on login form
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(){
        Auth::logout();
        return redirect()->route('auth.login');
    }
}
