<?php

namespace App\Http\Requests;

use App\Rules\GPXValidation;
use Illuminate\Foundation\Http\FormRequest;

class TripRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => "required",
            'vehicle' => "required"
        ];

        if($this->method() === "POST"){
            $rules['points'] = ["required", new GPXValidation()];
        } else {
            $rules['points'] = [new GPXValidation()];
        }

        return $rules;
    }
}
