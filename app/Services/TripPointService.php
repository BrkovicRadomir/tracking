<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 1/1/19
 * Time: 4:48 PM
 */

namespace App\Services;
use phpGPX\phpGPX;
use App\TripPoint;
use App\Exceptions\GPXIncorrectFormatException;

class TripPointService
{

    /**
     * @param $tripId
     * @param $file
     * @throws GPXIncorrectFormatException
     */
    public static function create($tripId, $file){
        $message = "The ".$file->getClientOriginalName()." hasn't expected format";
        $gpx = new phpGPX();
        $file = $gpx->load($file);

        if($file->tracks && is_array($file->tracks)){

            if($file->tracks[0]->segments){
                if($file->tracks[0]->segments[0]->points){
                    foreach ($file->tracks[0]->segments[0]->points as $point){
                        TripPoint::create([
                           'trip_id' => $tripId,
                           'latitude' => $point->latitude,
                           'longitude' => $point->longitude,
                           'elevation' => $point->elevation,
                           'time' => $point->time,
                        ]);
                    }
                } else {
                    throw new GPXIncorrectFormatException($message);
                }
            } else {
                throw new GPXIncorrectFormatException($message);
            }

        } else {
            throw new GPXIncorrectFormatException($message);
        }
    }
}