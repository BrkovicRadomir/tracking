<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripPoint extends Model
{
    protected $table = "trip_points";
    protected $guarded = ['id'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public  function trip(){
        return $this->belongsTo(Trip::class, 'trip_id', 'id');
    }
}
