# Application for tracking trips

This is test application for tracking trips. 
When you complete registration, you will be able to create your trip.   
Firstly you have to write name of your trip, then you have to choose vehicle type, and at the last you have to upload GPS file. 

After then you will see new trip in your trips list. Each trip in list has three actions:

- Show (display single trip page with google maps and simple GPS path)
- Edit (edit information about trip. If you upload new GPS file, application will remove all old points and create new points)
- Delete (delete trip and GPS path)


## Environment

- Framework: Laravel version - 5.7.19
- Database: MySQL version - 5.7
- PHP: 7.2
- Server: Apache 2.4.18
- Composer: 1.5


## Installation guide

When you clone this repository, you will have to make .env file with environment variables, and make database. Then you have to run console command: 
```composer install```, after that run console command ```php artisan migrate```. In the end run command ```php artisan serve```, and then application will be ready for using.  
